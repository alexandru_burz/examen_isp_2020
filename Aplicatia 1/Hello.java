import java.beans.IntrospectionException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicInteger;

public class Hello implements Runnable {
    Thread t1;
    Thread t2;
    Thread t3;
    AtomicInteger i;
    Hello(){
        t1 = new Thread(this,"VThread1");
        t2 = new Thread(this,"VThread2");
        t3 = new Thread(this,"VThread3");
        i = new AtomicInteger(0);
    }
    public void starter(){
        t1.start();
        t2.start();
        t3.start();
    }
    @Override
    public void run() {
        while(i.get()<11){
            Thread r = Thread.currentThread();
            if(r.getName().equals("VThread1")){
                if(i.get()%3 == 0){
                    System.out.println(r.getName() +" "+i.getAndAdd(1));
                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            if(r.getName().equals("VThread2")){
                if(i.get()%3 == 1){
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(r.getName() +" "+i.getAndAdd(1));
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            if(r.getName().equals("VThread3")){
                if(i.get()%3 == 2){
                    try {
                        Thread.sleep(4000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(r.getName() +" "+i.getAndAdd(1));
                }
            }
        }
    }

    public static void main(String[] args) {
        Hello nu = new Hello();
        nu.starter();
    }
}